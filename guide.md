---
layout: default
title: Guide
order: 4
---

# The simple guide to creating a new website

## 1. Pre-requisites
Make sure that you have access to our [LCSB's Gitlab instance - https://gitlab.lcsb.uni.lu/](https://gitlab.lcsb.uni.lu/) and log in. 

<small>If you have ever used it to clone a repository or create a new one, then you should be fine.  If you don't have the access, please open a ticket at [https://service.uni.lu](https://service.uni.lu) or contact the sysadmins (`lcsb-sysadmins (at) uni.lu`). </small>

## 2. Forking the example on Gitlab 
[Please click on this link to fork the example repository](https://gitlab.lcsb.uni.lu/core-services/pages-jekyll-lcsb-template/-/forks/new). You will be asked to specify a place to store the fork. You can just click on your personal namespace:
![image](assets/screenshots/step_one_fork.png)

## 3. Putting some content
In Gitlab, please launch *Web IDE* by clicking its button in the page:
![image](assets/screenshots/step_one_forked.png)

The Web IDE will launch - it's a useful online editor to make quick changes to the repository without cloning (downloading). We will make one small change in `index.md` file - please locate it on the left-hand-side and click it.

Please replace the contents of `index.md` with the following (use copy&paste):

```
---
layout: default
title: Home page
order: 1
---

# Hello, world
This is your personal page, generated using Jekyll and published by Gitlab CI and Gitlab Pages.

## This is a sub-header
You can use Markdown to format the text. Using one asterisk will *make the text italic*, using two asterisks **will make text bold**. You can create hyperlinks this syntax: [Google](https://www.google.com).

<pre>You could use HTML if you'd like, too!</pre>
```

and click "Commit..." button on the left-hand-side:

![image](assets/screenshots/step_two_content.png)

Then put `First commit` as a Commit message, select `Commit to master branch` and lick `Commit`:
![image](assets/screenshots/step_two_commit.png)


## 4. Almost there!
Technically, we are done! If you clicked the "Commit" button, Gitlab CI is already generating the website using Jekyll.

We can preview the process in the CI/CD page - go back to the main page of your fork (the same from the point #3), and choose "CI/CD" from the left-hand-side menu:
![image](assets/screenshots/step_three_ci.png)

Then click the little circle - it can be either green or blue depending on the progress (or red in case of some errors), and click _pages_ in the small box:
![image](assets/screenshots/step_three_ci_2.png)

Here you can see all the output from our automatic system. What's interesting for us, is the URI of the generated website - in our example it should be a couple lines from the end of the log:
![image](assets/screenshots/step_three_ci_3.png)

Click on the link (You might need to allow the security exception by clicking "Advanced" button, and clicking "Proceed...")...
![image](assets/screenshots/step_four_warnings.png)

...and this is your website! It should be accessible to everyone in the Uni.lu network (i.e. on work computers or via VPN).
![image](assets/screenshots/step_four_website.png)


## (5. Removing fork relationship)
It will help a little bit if we perform one more step to clean up.

a) In Gitlab, please choose *Settings/General* from the menu on the left hand side, and scroll all the way down to *Advanced*, and click "Expand":
![image](assets/screenshots/step_one_settings.png)

b) Scroll all the way down once again, and click red, scary button *Remove fork relationship*, and in the box that appears, type `pages-jekyll-lcsb-template` and click "Confirm".
![image](assets/screenshots/step_one_remove.png)

<p>&nbsp;</p>

#### Now it's time to go to Web IDE and create your own content :)
These were just first steps; hope it was very easy for you. There are couple of things you might want to know, too:
 * What are the next steps? How to finish configuration of the website?
 * How to run Jekyll locally on your machine to preview the changes before making them live?
 * How to make your website available to everyone in the Internet (e.g. like <https://howto.lcsb.uni.lu/> which is publicly available)?
 * How to add images or youtube videos?
 * Is there a way to start the website without forking?
 * What will happen if you don't remove fork relationship?
 * _and many more..._

**We anwser these questions [in the manual](howto).**

<p>&nbsp;</p>

# Was there any problem?
Please, either:
 * let us know by sending an email to `lcsb-sysadmins (at) uni.lu`
 * or take a look on our [troubleshooting guide](help)
