---
layout: default
title: How-to manual
order: 6
---


 * #### How to finish configuration of the website?
Remove the unnecessary files - screenshots (`assets/screenshots`) and markdown files (`howto.md`, `help.md`, `guide.md`, `about.md` - but before deleting you might want to take a peek to familiarize with Markdown syntax). Open `_config.yml` and update the metadata with the help from the comments. Add new content, then commit and push to the repository.

 * #### How to run Jekyll locally on your machine to preview the changes before making them live?
You need to have Ruby installed on your machine, and bundler (`gem install bundler`). Clone the repository and open it in terminal. Run `bundle install` - which will finish the installation. <br /> <br />  Now, every time you'd like to preview the website, use `bundle exec jekyll serve` in terminal, and click the link that appears to view the webpage. It will update automatically every time you save the new content, too (you just need to refresh the page in the browser) - except when you change the `_config.yml` (which requires restarting `bundle exec jekyll serve`).

 * #### How to make your website available to everyone in the Internet (e.g. like <https://howto.lcsb.uni.lu/> which is publicly available)?
Please contact us. We need to ask SIU to create some redirections in Uni.lu network. It will normally take 1 to 3 days.

 * #### How to add images or youtube videos?
Normally, you can put images to `assets` directory, and then use this syntax: `![image](assets/filename.png)`. For embedding youtube videos, you need to embed HTML code.

 * #### Is there a way to start the website without forking?
Yes, naturally! Technically, you can start a new project in Gitlab, and just commit and push a couple of files - `_config.yml`, `Gemfile`, `index.md`, `.gitlab-ci.yml` (with the appropriate content). These are bare minimum to have a working website.

 * #### What will happen if you don't remove fork relationship?
Every time you open Merge Requests, your default choice will point to our template. It's annoying for you, and we don't really want that (unless you'd like to help us improve it with your changes :)).

 * #### If you want to have the banner link to different target (e.g. uni.lu's index site)
Uncomment `banner_link: https://wwwen.uni.lu/` in the `_config.yml`, and change it to your target URI.
